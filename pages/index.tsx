import type { NextPage } from "next";
import { motion } from "framer-motion";
import { useTransform } from "framer-motion";
import { useScroll } from "framer-motion";
import { useEffect, useState } from "react";

const Home: NextPage = () => {
    const { scrollY } = useScroll();
    const [showSecond, setShowSecond] = useState<Number>(0);

    useEffect(() => {
        return scrollY.onChange((latest) => {
            setShowSecond(latest);
            console.log("Page scroll: ", latest);
        });
    }, []);

    return (
        <motion.div animate={{}} className=" space-y-6 bg-black  ">
            <motion.div className="flex relative flex-col h-[100vh] items-center justify-center  bg-black ">
                <video muted autoPlay width="60%" height="60%" src="https://www.apple.com/105/media/us/ipad-air/2022/5abf2ff6-ee5b-4a99-849c-a127722124cc/anim/hero/large_2x.mp4"></video>
                <motion.img src="https://www.apple.com/v/ipad-air/r/images/overview/hero/main_ipad__d991v5y9hgom_large.png" className=" absolute w-2/3 h-2/3 bg-transparent  " alt="" />
            </motion.div>
            <div className="relative">
                <video src="air.mp4" className="opacity-30" muted height={"100vh"} autoPlay loop id="myVideo"></video>
                <div className="space-y-10 absolute top-0 left-0 w-[100vw]">
                    {showSecond > 200 ? (
                        <motion.div initial={{ y: 200, opacity: 0 }} className=" w-full pb-6 mb-[70px] flex items-center justify-center" animate={{ opacity: 1, y: 0 }} transition={{ duration: 2 }}>
                            <motion.h1 className="text-9xl font-medium text-center align-middle text-transparent bg-clip-text   bg-gradient-to-r text-clip from-[#dc79ff] to-[#256bfa]">
                                Light. Bright. <br /> Full of might.
                            </motion.h1>
                        </motion.div>
                    ) : (
                        <motion.div className=" w-full pb-6 mb-[70px] flex items-center justify-center">
                            <motion.h1 className="text-9xl font-medium text-center align-middle "></motion.h1>
                        </motion.div>
                    )}
                    {showSecond > 400 ? (
                        <motion.div className="text-white w-full pb-6 flex items-center justify-center" animate={{ opacity: 1, y: 0 }} initial={{ y: 200, opacity: 0 }} transition={{ duration: 2 }}>
                            <motion.h1 className="text-[72px] font-medium text-center align-middle  bg-clip-text  ">
                                Supercharged by the <br /> Apple M1 chip.
                            </motion.h1>
                        </motion.div>
                    ) : (
                        <motion.div className=" w-full pb-6 mb-[70px] flex items-center justify-center">
                            <motion.h1 className="text-9xl font-medium text-center align-middle "></motion.h1>{" "}
                        </motion.div>
                    )}

                    {showSecond > 600 ? (
                        <motion.div className="text-white w-full pb-6 flex items-center justify-center" animate={{ opacity: 1, y: 0 }} initial={{ y: 200, opacity: 0 }} transition={{ duration: 2 }}>
                            <motion.h1 className="text-[72px] font-medium text-center align-middle  bg-clip-text   ">
                                12MP Ultra Wide front camera <br /> with Center Stage.
                            </motion.h1>
                        </motion.div>
                    ) : (
                        <motion.div className=" w-full pb-6 mb-[70px] flex items-center justify-center">
                            <motion.h1 className="text-9xl font-medium text-center align-middle "></motion.h1>{" "}
                        </motion.div>
                    )}

                    {showSecond > 800 ? (
                        <motion.div className="text-white w-full pb-6 flex items-center justify-center" animate={{ opacity: 1, y: 0 }} initial={{ y: 200, opacity: 0 }} transition={{ duration: 2 }}>
                            <motion.h1 className="text-[72px] font-medium text-center align-middle  bg-clip-text    ">Blazing-fast 5G.</motion.h1>
                        </motion.div>
                    ) : (
                        <motion.div className=" w-full pb-6 mb-[70px] flex items-center justify-center">
                            <motion.h1 className="text-9xl font-medium text-center align-middle "></motion.h1>{" "}
                        </motion.div>
                    )}

                    {showSecond > 1000 ? (
                        <motion.div className="text-white w-full pb-6 flex items-center justify-center" animate={{ opacity: 1, y: 0 }} initial={{ y: 200, opacity: 0 }} transition={{ duration: 2 }}>
                            <motion.h1 className="text-[72px] font-medium text-center align-middle  bg-clip-text    ">Five gorgeous colors.</motion.h1>
                        </motion.div>
                    ) : (
                        <motion.div className=" w-full pb-6 mb-[70px] flex items-center justify-center">
                            <motion.h1 className="text-9xl font-medium text-center align-middle "></motion.h1>{" "}
                        </motion.div>
                    )}

                    <div className="flex gap-7 justify-center text-xl text-blue-600 font-normal">
                        <p>Watch the film</p>
                        <p>Watch the event</p>
                    </div>
                </div>
            </div>
        </motion.div>
    );
};

export default Home;
