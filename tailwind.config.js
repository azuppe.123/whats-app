/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
    theme: {
        extend: {
            fontFamily: {
                "Josefin": ["Josefin Sans"],
              },
            colors: {
                "pink-200": "#FEDADA",
                "yellow-200": "#FDFEDA",
                "green-200": "#E1ECE1",
                "blue-200": "#DAFEF5",
                "purple-200": "#F5DAFE",
                "lightgreen-200": "#DAFEE8",
            },
        },
    },
    plugins: [],
};
